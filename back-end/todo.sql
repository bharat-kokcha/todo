-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2019 at 09:36 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('20623882d8fd11b985aa6683a41d6c23cebfb2b97f5384b4bcd5589e7b9151ef6715aac13544c0b1', 1, 1, 'MyApp', '[]', 0, '2019-09-16 13:41:09', '2019-09-16 13:41:09', '2020-09-16 19:11:09'),
('2eaac0967ddc671de31cffefa73ed6d13466a848fac63fafc28ab16212e20f256840b5bbabab140d', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:28:43', '2019-09-17 11:28:43', '2020-09-17 16:58:43'),
('30b72a6445a9753d1ab9c66c207c7490682eb96fdabd373430f6337213e004cbf398d4221272e532', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:47:18', '2019-09-17 11:47:18', '2020-09-17 17:17:18'),
('3e152c049ea5af5fe9635d8ad9f9cc01b6857882ce9b5c8d46d87d4ad3a29f5e1125d626c59ff409', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:25:30', '2019-09-17 11:25:30', '2020-09-17 16:55:30'),
('401dacae41ea9fdfead6f1269758cceb0590456c274bef2f1c6464504678046986fcb3c1bd6ce561', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:10:53', '2019-09-17 11:10:53', '2020-09-17 16:40:53'),
('4ecfb0fab90447f4d834a06be96ce44c3a3511e8c55f05d1bfb946139417c98893806fb2ae0fe35f', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:37:47', '2019-09-17 11:37:47', '2020-09-17 17:07:47'),
('5a67cda11d4b769e4eb9008bb80860bb13d926de9bae4e0db6ac2e4b0d09321a4cbaba1af2a2b9bd', 1, 1, 'MyApp', '[]', 0, '2019-09-16 13:41:41', '2019-09-16 13:41:41', '2020-09-16 19:11:41'),
('65dcccab9479c290729984bba98ee3e3477614915ad3d94ca91fcdf7f169a580557e6654c8610d87', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:37:05', '2019-09-17 11:37:05', '2020-09-17 17:07:05'),
('6a50b21239d8990fe77e3b4040587b702800df06845d39c1c36c20ed1838869a48efa3ca1e57c98c', 1, 1, 'MyApp', '[]', 0, '2019-09-16 13:39:26', '2019-09-16 13:39:26', '2020-09-16 19:09:26'),
('749a93788e850e2a99ffc4a7fb8e720cfd94f6fd513dc922d16f3d557861ce503ff6474daaf91c00', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:23:14', '2019-09-17 11:23:14', '2020-09-17 16:53:14'),
('a48df76592a75267ff1272baea5ae73c3e8623c7b45f8bf5669e96c53b74f5857fe287a889c1cf44', 2, 1, 'MyApp', '[]', 0, '2019-09-17 11:42:56', '2019-09-17 11:42:56', '2020-09-17 17:12:56'),
('b16ec437cb07c3c2e2fdd7e54afc27c5b76cfbef213f392d31029e735278a32c587d0ddc7c01c94e', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:49:42', '2019-09-17 11:49:42', '2020-09-17 17:19:42'),
('d9ca25f7ce69b8faefdc18b9dbed7fcbe6d7293af4b72dea21edb304b01a0013eb3f88d748236b39', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:46:17', '2019-09-17 11:46:17', '2020-09-17 17:16:17'),
('f86152fda0595c778c8319286e879eb374cb1ca204aa4d103f108d485201f6a21111a70f560525a9', 1, 1, 'MyApp', '[]', 0, '2019-09-17 11:28:11', '2019-09-17 11:28:11', '2020-09-17 16:58:11');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'eGRXCpmgg0IbUnj13vZtaEAHCSDbRT1eYmLm0exB', 'http://localhost', 1, 0, 0, '2019-09-16 13:30:02', '2019-09-16 13:30:02'),
(2, NULL, 'Laravel Password Grant Client', 'aFgLHaGDwlB5cCQ5jImnP2cCgMOVwhG5bodfCbG8', 'http://localhost', 0, 1, 0, '2019-09-16 13:30:02', '2019-09-16 13:30:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-09-16 13:30:02', '2019-09-16 13:30:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `todo`
--

CREATE TABLE `todo` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `task` varchar(255) DEFAULT NULL,
  `status` enum('Pending','Complete','Inprocess') NOT NULL DEFAULT 'Pending',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `todo`
--

INSERT INTO `todo` (`id`, `user_id`, `task`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 'Update record', 'Complete', '2019-09-17 03:05:21', '2019-09-17 03:05:56'),
(3, 1, 'Test detail', 'Pending', '2019-09-17 03:05:21', '2019-09-17 03:05:56'),
(4, 1, 'hshsdh', 'Pending', '2019-09-17 18:16:16', '2019-09-17 18:16:16'),
(5, 1, 'new task', 'Pending', '2019-09-17 18:18:57', '2019-09-17 18:18:57'),
(6, 1, 'Dev test', 'Pending', '2019-09-17 18:20:50', '2019-09-17 18:20:50'),
(7, 1, 'Dev Test', 'Pending', '2019-09-17 18:21:05', '2019-09-17 18:21:05'),
(8, 1, 'New s', 'Pending', '2019-09-17 18:21:34', '2019-09-17 18:21:34'),
(9, 1, 'dsds', 'Complete', '2019-09-17 18:22:03', '2019-09-17 18:50:23'),
(10, 1, 'sdds rtsts', 'Complete', '2019-09-17 18:22:36', '2019-09-17 18:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'david', 'david@test.com', NULL, '$2y$10$2tWebiUkhz8F6XiUSH3ZfuUtAcKo2MJ.6n/KVt3ibo226gjdR./1K', NULL, '2019-09-16 13:39:23', '2019-09-16 13:39:23'),
(2, NULL, 'menu@test.com', NULL, '$2y$10$/QZVZhT4eS8faNh269mG6uZ9CSxfHEOPDhwx22EeTeFuGUp1UnxGO', NULL, '2019-09-17 11:42:55', '2019-09-17 11:42:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `todo`
--
ALTER TABLE `todo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
