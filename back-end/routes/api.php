<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){

	Route::get('todo-list', 'Api\TodoController@index')->name('todo-list');

	Route::post('add-todo', 'Api\TodoController@add')->name('add-todo');

	Route::put('edit-todo/{id}', 'Api\TodoController@edit')->name('edit-todo');

	Route::delete('delete-todo/{id}', 'Api\TodoController@delete')->name('delete-todo');
});


