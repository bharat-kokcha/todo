<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $table = 'todo';

    //protected $with = ["users"];

    protected $fillable = ["user_id","status","task"];

    public function users() {
    	return $this->belongsTo('App\User','user_id');
    }
}




