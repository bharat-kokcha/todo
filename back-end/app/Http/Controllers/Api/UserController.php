<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Auth;

class UserController extends Controller
{

	public $successStatus = 200;
    public $errorStatus = 401;


    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $token =  $user->createToken('MyApp')->accessToken;
            $data = $user; 
            return response()->json(['token' => $token,'data' => $data,'msg'=>'Login successfully.','code'=>$this->successStatus]); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised','code'=> $this->errorStatus]); 
        } 
    }

    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email', 
            'password' => 'required', 
        ]);
		if ($validator->fails()) { 
            return response()->json(['msg'=>$validator->errors(),'code'=>$this->errorStatus]);            
        }

        $oldEmail = User::where('email',$request['email'])->first(); 
        if (!empty($oldEmail)) {
            return response()->json(['msg'=>'Email already exists','code'=>$this->errorStatus]);               
        }    


		$input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $token =  $user->createToken('MyApp')->accessToken; 
        $data =  $user;
		return response()->json(['token'=>$token,'data'=>$data,'msg'=>'Register successfully.','code'=>$this->successStatus]); 
    }

}
