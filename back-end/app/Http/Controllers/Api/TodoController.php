<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Todo;
use Validator;
use Auth;

class TodoController extends Controller
{
   	public $errorCode = 400;
   	public $succesCode = 200;

    // Fetch lists of login user tasks	
    public function index() {
		return response()->json(['todo'=>Todo::where('user_id',Auth::user()->id)->orderBy('created_at','Desc')->get(),'code'=>$this->succesCode]); 
	}

	// Add new task by login user
	public function add(Request $request) {
		$validator = Validator::make($request->all(), [ 
            'task' => 'required'
        ]);
		if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors(),'code'=>$this->errorCode]);            
        }

        // Add task 
        Todo::create([
        	'task' => $request['task'],
        	'user_id' => Auth::user()->id,
        ]);

        return response()->json(['success'=>'Task add successfully','code'=>$this->succesCode]);            
	}


	// Update or Complete existing task by login user
	public function edit(Request $request,$id) {
		$todo = Todo::findOrFail($id);
		$validator = Validator::make($request->all(), [
			'task' => 'required',
			'status' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json(['errors'=>$validator->messages(),'code'=>$this->errorCode]);	
		} else {
			$todo->update($request->all());

			if ($request['status'] != 'Pending') {
				$msg = "Todo complete successfully.";
			} else {
				$msg = "Todo update successfully.";
			}

			return response()->json(['success'=>$msg,'code'=>$this->succesCode]);
		}
	}

	// Delete task by login user
	public function delete($id) {
		Todo::find($id)->delete();
		return response()->json(['success'=>'Task delete successfully.','code'=>$this->succesCode]);
	}


}	
