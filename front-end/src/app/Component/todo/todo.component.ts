import { Component, OnInit } from '@angular/core';
import { TodoService } from '../../Services/todo/todo.service';
import { environment } from '../../../environments/environment';
import  { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr'; 
import { Router } from '@angular/router';


@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  taskForm: FormGroup;
  submittedLogin: boolean = false;

  todoLists : any;
  constructor(
    public todoService : TodoService,
    private formBuilder: FormBuilder,
    private toastr : ToastrService,
    private myRoute: Router
  ) { }

  ngOnInit() {
    this.taskFormValidation();
    this.todoList();
  }

  get l() { return this.taskForm.controls; } 

  taskFormValidation() {
    this.taskForm = this.formBuilder.group({
      task : ['',Validators.required],
      id : [''],
      status : ['Pending']
    });
  }

  taskFormSubmit() {
    this.submittedLogin = true;
    if(!this.taskForm.valid) {
      this.toastr.error("Please submit all details",'Error');
      return;
    } else {

      (<HTMLInputElement>document.querySelector('#addTask')).disabled = true;
      (<HTMLInputElement>document.querySelector('#addTask')).innerHTML = 'Please wait..' 

      if (this.taskForm.value.id == '') {
        this.todoService.addTaskForm(this.taskForm.value).subscribe(data => {
          (<HTMLInputElement>document.querySelector('#task')).value = "";
          (<HTMLInputElement>document.querySelector('#addTask')).disabled = false;
          (<HTMLInputElement>document.querySelector('#addTask')).innerHTML = 'Add Task' 
          if (data.code === 200) {
            this.toastr.success(data.success,'Success');
          } else {
            this.toastr.error(data.error,'Error');
          }
          this.todoList();
        });  
      } else {
        this.todoService.updateTaskForm(this.taskForm.value).subscribe(data => {
          (<HTMLInputElement>document.querySelector('#task')).value = "";
          (<HTMLInputElement>document.querySelector('#addTask')).disabled = false;
          (<HTMLInputElement>document.querySelector('#addTask')).innerHTML = 'Add Task' 
          if (data.code === 200) {
            this.toastr.success(data.success,'Success');
          } else {
            this.toastr.error(data.error,'Error');
          }
          this.todoList();
        });  
      }
      
      
      
    }  
  }
  
  todoList() {
    this.todoService.getTodo().subscribe(data => {
      this.todoLists = data.todo;
    });
  }  

  update(id : number) {
    let record = this;
    this.todoLists.filter(function(task) {
      if(id == task.id) {
        record.taskForm.controls.task.setValue(task.task);
        record.taskForm.controls.id.setValue(task.id);
        record.taskForm.controls.status.setValue('Pending');
        (<HTMLInputElement>document.querySelector('#task')).value = task.task;
        (<HTMLInputElement>document.querySelector('#addTask')).innerHTML = 'Edit Task' 
      }
    })
  }

  reset() {
    (<HTMLInputElement>document.querySelector('#addTask')).innerHTML = 'Add Task'; 
  }

  complete(id : number) {
    let record = this;
    this.todoLists.filter(function(task) {
      if(id == task.id) {
        record.taskForm.controls.task.setValue(task.task);
        record.taskForm.controls.id.setValue(task.id);
        record.taskForm.controls.status.setValue('Complete');
        (<HTMLInputElement>document.querySelector('#task')).value = '';
        (<HTMLInputElement>document.querySelector('#addTask')).innerHTML = 'Add Task' 

        record.todoService.updateTaskForm(record.taskForm.value).subscribe(data => {
          record.taskForm.controls.task.setValue('');
          record.taskForm.controls.id.setValue('');
          record.taskForm.controls.status.setValue('Pending');
          (<HTMLInputElement>document.querySelector('#addTask')).disabled = false;
          (<HTMLInputElement>document.querySelector('#addTask')).innerHTML = 'Add Task' 
          if (data.code === 200) {
            record.toastr.success(data.success,'Success');
          } else {
            record.toastr.error(data.error,'Error');
          }
          record.todoList();
        });
      }
    })
  }

  delete(id : number) {
    this.todoService.deleteTaskForm(id).subscribe(data => {
      if (data.code === 200) {
        this.toastr.success(data.success,'Success');
      } else {
        this.toastr.error(data.error,'Error');
      }
      this.todoList();
    });
  }
  
  logout() {
    this.myRoute.navigate(["login"]);  
  }
  
}
