import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../Services/common/common.service';
import { environment } from '../../../environments/environment';
import  { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr'; 
import { AuthService } from '../../Services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submittedLogin: boolean = false;
  

  constructor(
    public common : CommonService,
    private formBuilder: FormBuilder,
    private toastr : ToastrService,
    private auth: AuthService,
    private myRoute: Router
  ) { }

  ngOnInit() {
    if (localStorage.getItem("LoggedInUser") !== null) {
      localStorage.removeItem("LoggedInUser");
      localStorage.removeItem("LoggedInfo");
      localStorage.removeItem("StudentInfo");
      window.location.reload();
    }

    this.loginFormValidation();
  }

  get l() { return this.loginForm.controls; } 

  loginFormValidation() {
    this.loginForm = this.formBuilder.group({
      email : ['',[Validators.required,Validators.email]],
      password : ['',Validators.required],
    });
  }

  loginFormSubmit() {
    this.submittedLogin = true;
    if(!this.loginForm.valid) {
      this.toastr.error("Please submit all details",'Error');
      return;
    } else {

      (<HTMLInputElement>document.querySelector('#loginBtn')).disabled = true;
      (<HTMLInputElement>document.querySelector('#loginBtn')).value = 'Please wait..' 
      
      this.common.submitLoginForm(this.loginForm.value).subscribe(data => {
        
        if(data.code === 401) {
          (<HTMLInputElement>document.querySelector('#loginBtn')).disabled = false;
          (<HTMLInputElement>document.querySelector('#loginBtn')).value = 'Login'
          this.toastr.error(data.error,'Error');
        } else {
          console.log(data);
          this.auth.sendToken(data.token)
          this.auth.storeDetail(data.data);
          
          this.toastr.success(data.msg,'Success');
          this.myRoute.navigate(["to-do"]);
        }
      })  
    }  
  }
}
