import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../Services/common/common.service';
import { environment } from '../../../environments/environment';
import  { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { MustMatch } from '../../helper/must-match.validator';
import { ToastrService } from 'ngx-toastr'; 
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  registerForm: FormGroup;
  submitted: boolean = false;
  
  constructor(
    private commonServices : CommonService,
    private formBuilder: FormBuilder,
    private toastr : ToastrService,
    private myRoute: Router
    
    
  ) { }

  ngOnInit() {
    this.registerFormValidation();
  }

  get f() { return this.registerForm.controls; }
  

  registerFormValidation() {
     this.registerForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
        confirm_password: ['', Validators.required],
    }, {
        validator: [MustMatch('password', 'confirm_password')],
    });
  }

   // Submit register form 
   onSubmit() {

    this.submitted = true;

    if(!this.registerForm.valid) {
      return ;
    } else {
      (<HTMLInputElement>document.querySelector('#submitBtn')).disabled = true;
      (<HTMLInputElement>document.querySelector('#submitBtn')).value = 'Please wait..' 
      
      this.commonServices.submitRegisterForm(this.registerForm.value).subscribe(data => {
        if(data.code === 401) {
          (<HTMLInputElement>document.querySelector('#submitBtn')).disabled = false;
          (<HTMLInputElement>document.querySelector('#submitBtn')).value = 'Register' 
          this.toastr.error(data.msg,'Error');
        } else {
          this.toastr.success(data.msg,'Success');
          this.registerForm.reset();
          (<HTMLInputElement>document.querySelector('#submitBtn')).disabled = false;
          (<HTMLInputElement>document.querySelector('#submitBtn')).value = 'Register'
          this.myRoute.navigate(["login"]); 
        }
      });
    }
  }  
  

}
