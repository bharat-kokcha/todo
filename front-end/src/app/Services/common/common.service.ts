import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private apiUrl = environment.apiUrl;
  
  constructor(
    private http: HttpClient,
  ) { }

  submitRegisterForm (data) : Observable<any> {
    return this.http.post<any>(this.apiUrl+'register',data);
  }
  
  submitLoginForm(data): Observable<any> {
    return this.http.post<any>(this.apiUrl+'login',data);
  }
}
