import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  basicPath = environment.apiUrl;

  constructor(private myRoute: Router) {}
  
  sendToken(token: string) {
    localStorage.setItem("LoggedInUser", token)
  }
  getToken() {
    return localStorage.getItem("LoggedInUser")
  }
  isLoggedIn() {
    return this.getToken() !== null;
  }
  logout() {
    localStorage.removeItem("LoggedInUser");
    localStorage.removeItem("LoggedInfo");
    
    // this.myRoute.navigate([""]);

  }

  storeDetail(data) {
    localStorage.setItem("LoggedInfo", JSON.stringify(data));
  }
  
  getStoreDetail() {
    return localStorage.getItem("LoggedInfo");
  }

  
}
