import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { AuthService } from '../auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private auth : AuthService
  ) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer '+ this.auth.getToken()
    })
  };

  getTodo() : Observable <any> {
    return this.http.get<any>(this.apiUrl+'todo-list',this.httpOptions);
  } 

  addTaskForm (data) : Observable<any> {
    return this.http.post<any>(this.apiUrl+'add-todo',data,this.httpOptions);
  }

  updateTaskForm (data) : Observable<any> {
    return this.http.put<any>(this.apiUrl+'edit-todo/'+data.id,data,this.httpOptions);
  }

  deleteTaskForm (id) : Observable<any> {
    return this.http.delete<any>(this.apiUrl+'delete-todo/'+id,this.httpOptions);
  }


}
